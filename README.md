# ``Job Shop Scheduling`` 

Ce projet se concentre sur le problème de l'ordonnancement d'ateliers, également connu sous le nom de "Job Shop Scheduling". 

Il s'agit d'un problème d'optimisation combinatoire qui se pose dans la planification de la production dans des ateliers de fabrication où plusieurs machines doivent être utilisées pour traiter diverses tâches.

## ``Problème``

Le problème de l'ordonnancement d'ateliers consiste à déterminer l'ordre d'exécution des tâches (jobs) sur un ensemble de machines tout en respectant certaines contraintes, telles que :

1. Chaque tâche doit être traitée sur une machine spécifique.
2. Une machine ne peut traiter qu'une tâche à la fois.
3. Les tâches doivent être exécutées sans interruption sur une machine.

Voici le code complet pour un README en Markdown expliquant le projet :

## ``Structure du répertoire``
```
- /src : Contient le code source principal.
- /data : Contient les fichiers de données d'entrée pour le problème.
- /output : Où les résultats seront stockés.
- /include : Fichiers d'en-tête pour le projet.
- /utils : Fichiers utilitaires pour l'affichage et l'initialisation.
```
## Prérequis
```
- C++ 
- Visual Studio Code
```
## Détails du code

Le code est organisé comme suit :

### `jobshop.cpp`

Ce fichier contient la logique principale pour résoudre le problème du Job Shop.

### `display-utility.cpp`

Ce fichier contient des fonctions d'affichage pour présenter les résultats.

### `initializer-utility.cpp`

Ce fichier contient des fonctions pour initialiser les données.

## ``Fonctionnalités clés``

- Utilisation de l'``algorithme GRASP`` pour résoudre le problème.
- Utilisation de structures de données avancées (`T_LAB` et `T_SOLUTION`) pour représenter les tâches et les machines.
