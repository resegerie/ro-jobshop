#ifndef __TP2_RO__
#define __TP2_RO__

#include <string>
#include <vector>
#include <tuple>
#include <random>

#include "MersenneTwister.h"

#define NMAX 50
#define MMAX 50
#define MAX_LAMBDA 150

#define INF INT_MAX

#define DEBUG_SEED 42

const int imax = 100;
const int jmax = 100;

static int counter = 0;


using namespace std;

struct T_SOLUTION {
    int size;
    int id      = ++counter;
    int cost;

    int lambda[MAX_LAMBDA];
    int startTime[NMAX][MMAX];
    vector<vector<tuple<int, int>>> pere;
};

struct T_LAB {
    string lab;
    int nbPiece;
    int nbMachine;
    vector<vector<int>> machine;
    vector<vector<int>> processingTime;
};

// Functions
vector<T_LAB> collectJobShop             (string filename);

void          evaluate                   (T_LAB& lab, T_SOLUTION& solution);
int           localSearch                (T_SOLUTION& solution, T_LAB& lab, int maxIter);

int           GRASP                      (T_LAB& lab,int maxIter, int maxNoImprovement, MersenneTwister *mt);

int           getPosition                (int job, int opNum, const int* lambda, int size);
int           calculateHash              (T_SOLUTION& solution);
void          swapLambda                 (int *lambda, int index1, int index2, int size, int *newLambda);

void          writeGanttChartToFile      (const T_SOLUTION& solution, const T_LAB& lab, const std::string& outputFilename);

#endif
