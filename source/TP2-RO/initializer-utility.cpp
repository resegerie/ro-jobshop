#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include "jobshop.h"
#include <algorithm>


#include "MersenneTwister.h"



void initializeStVector(T_SOLUTION& solution, T_LAB& lab)
{
    for (int i = 0; i < lab.nbPiece; i++)
    {
        for (int j = 0; j < lab.nbMachine; j++)
        {
            solution.startTime[i][j] = 0;
        }
    }
}


T_SOLUTION initializeSol(int nbPiece, int nbMachine, int *lambda, int startTime[NMAX][MMAX]) {
    T_SOLUTION solution;

    solution.size = nbPiece * nbMachine;
    for (int i = 0; i < solution.size; i++) {
        solution.lambda[i] = lambda[i];
    }
    //solution.lambda = lambda;

    for (int i = 0; i < nbPiece; i++)
    {
        for (int j = 0; j < nbMachine; j++)
        {
            solution.startTime[i][j] = 0;
        }
    }
    return solution;
}

/*
void generateBiertwithVector(T_LAB& instance, T_SOLUTION& solution)
{
    int nbMachine = instance.nbMachine;
    int nbPiece = instance.nbPiece;
    solution.size = nbMachine * nbPiece;

    solution.lambda.resize(solution.size);  // Resize the lambda vector to the correct size

    int T[NMAX];
    int N[NMAX];
    int index;
    int bound = nbPiece;

    for (int i = 0; i < nbPiece; i++)
    {
        T[i] = i;
        N[i] = nbMachine;
    }

    for (int i = 0; i < solution.size; i++)
    {
        if (bound != 0) {
            index = rand() % bound;
        }
        solution.lambda[i] = T[index];  // Assign the random value to solution.lambda[i]

        if (--N[index] == 0)
        {
            bound--;
            N[index] = N[bound];
            T[index] = T[bound];
        }

    }
}*/

int uniform(int a, int b, MersenneTwister *mt)
{
    int r;
    r = mt->genrand_int31();
    return a + (r % (b - a + 1));
}


void generateBiertwithVector(T_LAB& instance, T_SOLUTION& solution, MersenneTwister *mt) {
    int nbMachine = instance.nbMachine;
    int nbPiece = instance.nbPiece;
    solution.size = nbMachine * nbPiece;

    //solution.lambda.resize(solution.size);

    int Tr[NMAX];
    int Nr[NMAX];
    int index;
    int bound = nbPiece;
    
    for (int i = 0; i < nbPiece; i++) {
        Tr[i] = i;
        Nr[i] = nbMachine;
    }

    for (int i = 0; i < solution.size; i++) {
        if (bound != 0) {
            //std::uniform_int_distribution<int> dist(0, bound - 1);  // Cr�ez une distribution pour g�n�rer des nombres al�atoires
            //index = dist(mt);  // G�n�rez un index al�atoire en utilisant le g�n�rateur Mersenne Twister
            index = uniform(0, bound - 1, mt);
        }
        solution.lambda[i] = Tr[index];

        if (--Nr[index] == 0) {
            bound--;
            Nr[index] = Nr[bound];
            Tr[index] = Tr[bound];
        }
    }
}


vector<T_LAB> collectJobShop(string filename)
{
    ifstream infile(filename);

    if (!infile)
    {
        cerr << "Erreur lors de l'ouverture du fichier." << endl;
    }

    vector<T_LAB> instances;
    string line;

    while (getline(infile, line))
    {
        if (line.find("instance la") != string::npos)
        {
            T_LAB instance;
            instance.lab = line.substr(line.find("la"), 5);
            getline(infile, line);
            istringstream iss(line);
            iss >> instance.nbPiece;
            iss >> instance.nbMachine;

            // Utilisation du constructeur pour initialiser les tableaux
            instance.machine        = vector<vector<int>>(instance.nbPiece, vector<int>(instance.nbMachine, 0));
            instance.processingTime = vector<vector<int>>(instance.nbPiece, vector<int>(instance.nbMachine, 0));

            for (int i = 0; i < instance.nbPiece; i++)
            {
                for (int j = 0; j < instance.nbMachine; j++)
                {
                    infile >> instance.machine[i][j];
                    infile >> instance.processingTime[i][j];
                }
            }

            getline(infile, line);
            instances.push_back(instance);
        }
    }
    return instances;
}
