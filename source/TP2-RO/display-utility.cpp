#include <iostream>
#include <random>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include "jobshop.h"

/**
 * @brief Display the Bierwith vector from a solution.
 *
 * This function displays the Bierwith vector, which represents the sequence of operations
 * for a given solution.
 *
 * @param solution The solution to display the Bierwith vector from.
 */
void displayBierwithVector(const T_SOLUTION& solution) {
    cout << "\nBierwith vector : " << endl;

    for (int i = 0; i < solution.size; i++) {
        cout << " " << (solution.lambda[i] + 1) << " ";
    }
}

/**
 * @brief Display the start times for each job on each machine.
 *
 * This function displays the start times for each job on each machine in the given solution.
 *
 * @param solution The solution to display start times from.
 * @param lab The job-shop instance containing machine and processing time information.
 */
void displayStVector(const T_SOLUTION& solution, const T_LAB& lab) {
    int size = solution.size;
    cout << "\nStart time for each jobs : " << endl;

    for (int i = 0; i < lab.nbPiece; i++) {
        for (int j = 0; j < lab.nbMachine; j++) {
            cout << " " << solution.startTime[i][j] << " ";
        }
        cout << endl;
    }
}

/**
 * @brief Display the parent (pere) relationships in a solution.
 *
 * This function displays the parent (pere) relationships for each operation in the given solution.
 *
 * @param solution The solution to display parent relationships from.
 * @param lab The job-shop instance containing machine and processing time information.
 */
void displayPere(const T_SOLUTION& solution, T_LAB& lab) {
    int job, machine;

    for (int j = 0; j < lab.nbPiece; j++) {
        for (int m = 0; m < lab.nbMachine; m++) {
            cout << "Parent of : (" << j << ", " << m << ") -->";

            job = get<0>(solution.pere[j][m]);
            machine = get<1>(solution.pere[j][m]);

            cout << "(" << job << ", " << machine << ") ";
        }
        cout << endl;
    }
}

/**
 * @brief Display the job-shop instance data.
 *
 * This function displays the data of a job-shop instance, including the machine numbers and
 * processing times for each job and machine.
 *
 * @param instance The job-shop instance to display.
 */
void displayJobShop(const T_LAB& instance) {
    for (int i = 0; i < instance.nbPiece; i++) {
        for (int j = 0; j < instance.nbMachine; j++) {
            cout << instance.machine[i][j] << " ";
            cout << instance.processingTime[i][j] << " ";
        }
        cout << endl;
    }
}

