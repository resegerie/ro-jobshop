#include <iostream>
#include <random>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include <set>

#include "jobshop.h"
#include "display-utility.h"
#include "initializer-utility.h"
#include "MersenneTwister.h"

#include <iostream>
#include <fstream>
#include <cstdlib>  

/**
 * @brief Write a Gantt chart representation of a solution to a file.
 *
 * This function generates a Gantt chart for each machine in a lab and writes it to a file.
 *
 * @param solution The solution object containing scheduling information.
 * @param lab The lab object representing the manufacturing environment.
 * @param outputFilename The name of the file where the Gantt chart will be written.
 */
void writeGanttChartToFile(const T_SOLUTION& solution, const T_LAB& lab, const std::string& outputFilename)
{
    // Open the output file for writing
    std::ofstream outputFile(outputFilename);

    // Loop through each machine
    for (int machineIndex = 0; machineIndex < lab.nbMachine; machineIndex++) {
        // Write the machine's header
        outputFile << "Machine " << machineIndex + 1 << " Gantt Chart:" << std::endl;

        // Loop through each job
        for (int job = 0; job < lab.nbPiece; job++) {
            int opNum = 0;

            // Find the next operation that belongs to the current machine
            while (lab.machine[job][opNum] != machineIndex)
            {
                opNum++;
            }

            // Write the job, operation, start time, and end time
            outputFile << "Job " << job + 1 << " - Operation " << opNum + 1 << ": ";
            outputFile << "Starts at " << solution.startTime[job][opNum] << " and ends at " << solution.startTime[job][opNum] + lab.processingTime[job][opNum] << std::endl;

            opNum++;
        }

        // Add a separator line between machines
        outputFile << "-----------------------------------------" << std::endl;
    }

    // Close the output file
    outputFile.close();
}


/**
 * @brief Calculate the cost of the longest path in the solution.
 *
 * @param solution The solution to be evaluated.
 * @param lab The job-shop instance.
 * @param longestPath The longest path in the solution.
 *
 * @return The cost of the longest path.
 */
int calculateLongestPathCost(T_SOLUTION& solution, T_LAB& lab, vector<tuple<int, int>> longestPath) 
{
    int cost = 0;

    for (const tuple<int, int>& task : longestPath) {
        cost += lab.processingTime[get<0>(task)][get<1>(task)];
    }

    return cost;
}

/**
 * @brief Find the longest path (critical path) in the solution.
 *
 * @param solution The solution to be evaluated.
 * @param lab The job-shop instance.
 *
 * @return The longest path as a vector of task tuples.
 */
vector<tuple<int, int>> findLongestPath(T_SOLUTION& solution, T_LAB& lab) 
{
    vector<tuple<int, int>> longestPath;

    const tuple<int, int> nullTuple = make_tuple(-1, -1);
    int task = -1;

    // Find the last task to finish (the one before *)
    int maxTime = INT_MIN;

    for (int i = 0; i < lab.nbPiece; i++) {
        int startTime = solution.startTime[i][lab.nbMachine - 1] + lab.processingTime[i][lab.nbMachine - 1];

        if (startTime > maxTime)
        {
            maxTime = startTime;
            task = i;
        }
    }


    int j = lab.nbMachine - 1;
    solution.cost = maxTime;

    tuple <int, int> p = make_tuple(task, j);

    // Here, we move up to the initial vertex with the help of the parent array
    // When we know the vertex that finishes last
    // We move to the previous vertex using the parent array

    while (p != nullTuple)
    {
        longestPath.push_back(p);
        p = solution.pere[task][j];
        task = get<0>(p);
        j = get<1>(p);
    }

 
    return longestPath;
}

/**
 * @brief Evaluate the solution by calculating the start times for each task.
 *
 * @param lab The job-shop instance.
 * @param solution The solution to be evaluated.
 */
void evaluate(T_LAB& lab, T_SOLUTION& solution) 
{
    int nJob[NMAX];
    tuple<int, int> nMachine[NMAX];

    const tuple<int, int> nullTuple = make_tuple(-1, -1);

    solution.pere.resize(lab.nbPiece, vector<tuple<int, int>>(lab.nbMachine, nullTuple));
    int j;

    for (int i = 0; i < lab.nbPiece; i++) 
    {
        nJob[i] = -1;
    }

    for (int i = 0; i < lab.nbMachine; i++) 
    {
        nMachine[i] = nullTuple;
    }

    for (int i = 0; i < solution.size; i++) 
    {
        j = solution.lambda[i];

        (nJob[j])++;

        if (nJob[j] > 0) 
        {
            int date = solution.startTime[j][nJob[j] - 1];
            int newDate = date + lab.processingTime[j][nJob[j] - 1];

            if (newDate > solution.startTime[j][nJob[j]]) 
            {
                solution.startTime[j][nJob[j]] = newDate;
                solution.pere[j][nJob[j]] = make_tuple(j, nJob[j] - 1);
            }
        }

        if (nJob[j] < lab.nbMachine) 
        {
            int machine = lab.machine[j][nJob[j]];

            if (nMachine[machine] != nullTuple) 
            {
                int opM = get<0>(nMachine[machine]);
                int opP = get<1>(nMachine[machine]);

                if (solution.startTime[opM][opP] + lab.processingTime[opM][opP] > solution.startTime[j][nJob[j]]) 
                {
                    solution.startTime[j][nJob[j]] = solution.startTime[opM][opP] + lab.processingTime[opM][opP];
                    solution.pere[j][nJob[j]] = make_tuple(opM, opP);
                }
            }
            nMachine[machine] = make_tuple(j, nJob[j]);
        }
    }
    findLongestPath(solution, lab);
}

/**
 * @brief Compute a local search on the current lab and solution.
 *
 * This function performs a local search on the provided solution and lab to improve the solution's cost.
 *
 * @param solution The solution to be improved.
 * @param lab The job-shop instance.
 * @param maxIter The maximum number of iterations for the local search.
 *
 * @return The cost of the improved solution.
 */
int localSearch(T_SOLUTION& solution, T_LAB& lab, int maxIter)
{
    int i, j;
    int iter = 0;
    int bestSolutionCost;

    vector<tuple<int, int>> path;

    tuple<int, int> currentTask;
    tuple<int, int> previousTask;
    int index1, index2;

    const tuple<int, int> nullTuple = make_tuple(-1, -1);
    tuple<int, int> pere;

    T_SOLUTION newSolution;

    evaluate(lab, solution);

    bestSolutionCost = solution.cost;
    path = findLongestPath(solution, lab);

    // Get the parent of the * vertex (last vertex)
    // It is at the head of longestPath
    currentTask = path.front();
    previousTask = solution.pere[get<0>(currentTask)][get<1>(currentTask)];

    while (iter <= maxIter && previousTask != nullTuple)
    {
        // If (x, y) = (x, z) ==> operation on the same piece
        if (get<0>(currentTask) == get<0>(previousTask))
        {
            // Move up => use longestPath because the path is already constructed
            currentTask = previousTask;
            previousTask = solution.pere[get<0>(currentTask)][get<1>(currentTask)];
        }
        else
        {
            index1 = getPosition(get<0>(currentTask), get<1>(currentTask), solution.lambda, solution.size);
            index2 = getPosition(get<0>(previousTask), get<1>(previousTask), solution.lambda, solution.size);

            int lambdaN[150];
            swapLambda(solution.lambda, index1, index2, solution.size, lambdaN);
            newSolution = initializeSol(lab.nbPiece, lab.nbMachine, lambdaN, solution.startTime);


            // We need to retrieve their positions in lambda
            evaluate(lab, newSolution);

            if (newSolution.cost < bestSolutionCost)
            {
                bestSolutionCost = newSolution.cost;
                solution = newSolution;
                path = findLongestPath(solution, lab);
                currentTask = path.front();
                previousTask = solution.pere[get<0>(currentTask)][get<1>(currentTask)];
            }
            else
            {
                // Move up in the parent tree
                currentTask = previousTask;
                previousTask = solution.pere[get<0>(currentTask)][get<1>(currentTask)];
            }
        }

        iter++;
    }

    return bestSolutionCost;
}




/**
 * @brief Run the GRASP algorithm.
 *
 * @param lab The job-shop instance.
 * @param maxIter The maximum number of iterations.
 * @param expectedBestCost The expected best cost.
 * @param mt Pointer to the Mersenne Twister random number generator.
 *
 * @return The best solution cost found by the algorithm.
 */
int GRASP(T_LAB& lab, int maxIter, int expectedBestCost, MersenneTwister* mt)
{
    int bestSolutionCost = INT_MAX;
    int exploredSolution[10000];

    T_SOLUTION bestSolution;
    int sol;
    int hash;

    int iter = 0;

    cout << "Expected : " << expectedBestCost << endl;
    cout << "Computing GRASP..." << endl;


    for (int i = 0; i < 10000; i++)
    {
        exploredSolution[i] = 0;
    }

    while (bestSolutionCost != expectedBestCost && iter < maxIter)
    {
        T_SOLUTION s;
        initializeStVector(s, lab);
        generateBiertwithVector(lab, s, mt);

        evaluate(lab, s);
        hash = calculateHash(s);

        if (!exploredSolution[hash])
        {
            sol = localSearch(s, lab, maxIter);
            if (sol < bestSolutionCost)
            {
                bestSolutionCost = sol;
                bestSolution = s;

                cout << "* New solution found : " << bestSolutionCost << endl;
            }
            exploredSolution[hash] = 1;
        }
        iter++;
        
    }

    // Writing the gantt chart for the best solution found (S*)
    string filename = "gantt-chart/gantt_chart_" + lab.lab + ".txt";
    writeGanttChartToFile(bestSolution, lab, filename);

    cout << "\n * GRASP - Bierwith vector associated to the best solution : " << endl;
    displayBierwithVector(bestSolution);

    return bestSolutionCost;
}

/**
 * @brief Calculate a hash value for the solution.
 *
 * @param solution The solution to be hashed.
 *
 * @return The hash value of the solution.
 */
int calculateHash(T_SOLUTION& solution) 
{
    int hash = 0;

    for (int i = 0; i < solution.size; i++)
    {
        for (int j = 0; j < solution.size; j++)
        {
            hash += solution.startTime[i][j] * solution.startTime[i][j];
        }
    }
    return hash % 10000;
}

/**
 * @brief Get the position of a task in the lambda vector.
 *
 * @param job The job number.
 * @param opNum The operation number.
 * @param lambda The lambda vector.
 * @param size The size of the lambda vector.
 *
 * @return The position of the task in the lambda vector.
 */
int getPosition(int job, int opNum, const int* lambda, int size) 
{
    int counter = -1;
    for (int i = 0; i < size; i++) 
    {
        if (lambda[i] == job) 
        {
            counter++;
            if (counter == opNum) 
            {
                return i;
            }
        }
    }
    return -1;
}

/**
 * @brief Swap two values in the lambda vector.
 *
 * @param lambda The lambda vector.
 * @param index1 The index of the first value to swap.
 * @param index2 The index of the second value to swap.
 * @param size The size of the lambda vector.
 * @param newLambda The updated lambda vector after swapping.
 */
void swapLambda(int* lambda, int index1, int index2, int size, int* newLambda) 
{
    for (int i = 0; i < size; i++) 
    {
        newLambda[i] = lambda[i];
    }
    // Store the value at index1 in a temporary variable
    int temp = newLambda[index1];
    newLambda[index1] = newLambda[index2];  // Copy the value at index2 to index1
    newLambda[index2] = temp;  // Place the temporary value in index2
}
