#ifndef __INITIALIZER__
#define __INITIALIZER__

#include "jobshop.h"
#include "MersenneTwister.h"

void          initializeStVector		(T_SOLUTION& solution, T_LAB& lab);
T_SOLUTION    initializeSol				(int nbPiece, int nbMachine, int* lambda, int startTime[NMAX][MMAX]);
vector<T_LAB> collectJobShop			(string filename);
void          generateBiertwithVector   (T_LAB& instance, T_SOLUTION& solution, MersenneTwister *mt);
int			  uniform					(int a, int b, MersenneTwister *mt);

#endif // !__INITIALIZER__
