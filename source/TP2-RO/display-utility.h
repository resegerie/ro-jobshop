#ifndef __DISPLAY_H__
#define __DISPLAY_H___

#include "jobshop.h"

void          displayBierwithVector		(const T_SOLUTION& solution);
void          displayStVector			(const T_SOLUTION& solution, const T_LAB& lab);
void          displayPere				(const T_SOLUTION& solution, T_LAB& lab);
void          displayJobShop			(const T_LAB& instance);

#endif // !__DISPLAY_H__
