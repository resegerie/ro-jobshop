/**
 * @file TP2-RO.cpp
 *
 * This file contains the main function. Program execution starts and ends here.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <chrono>
#include <random>

#include "jobshop.h"
#include "display-utility.h"
#include "initializer-utility.h"

#include "MersenneTwister.h"



 /**
  * @brief Main function of the program.
  *
  * @return Exit status of the program.
  */
int main() 
{
    // Seed used for repeatability
    // You can change the init to obtain an another seed
    uint32_t init[4] = { 0x333, 0x521, 0x124, 0x138 };
    uint32_t length = 4;

    // Create a Mersenne Twister generator
    MersenneTwister* mt2 = new MersenneTwister(init, length);

    // Contains different instances of Job-Shop
    vector<T_LAB> instances;

    // Timing execution
    auto start_time = std::chrono::high_resolution_clock::now();

    // Will hold the Bierwith vector(s) and the solution(s) found for instance(s)
    instances = collectJobShop("instances.txt");

    vector<tuple<int, int>> longestPath;

    int expectedBestCost[] = { 666, 655, 597, 590, 593, 926, 890, 863, 951, 958, 1222, 1039, 1150, 1292, 1207, 945, 784, 848, 842, 902 };

    int i = 1;
    for (T_LAB instance : instances) 
    {
        T_SOLUTION s;
        cout << " \nID solution : " << instance.lab << endl;

        // Change the number of iteration for GRASP to reach another solutions
        int solRecord = GRASP(instance, 2000, expectedBestCost[i - 1], mt2);
        cout << "\n* GRASP - Best solution cost : " << solRecord << endl;

        cout << "---------------------------------------------------" << endl;
        i++;
    }

    auto end_time  = std::chrono::high_resolution_clock::now();

    auto duration  = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    double seconds = duration.count() / 1e6;

    std::cout << "Execution time: " << seconds << " seconds" << std::endl;

    return 0;
}
